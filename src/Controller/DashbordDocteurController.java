/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConnectionDB.ConnectionClass;
import Model.Consultation;
import Model.Doctor;
import Model.Patient;
import ModelSys.FxmlLoader;
import ModelSys.Session;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author nadhira
 */
public class DashbordDocteurController implements Initializable {

    @FXML
    private Label lblName,lblPname,lblErrors,lblEdit;
    
    @FXML
    private TreeTableView<Consultation> treeTableView;
    @FXML
    private TreeTableColumn<Consultation, String> nameCol,reportCol,malCol,presCol,cureCol;
    @FXML
    private TextField txtMalady;
    @FXML
    private TextArea txtReport,txtPresp;
    @FXML
    private Button btnCancel,btnEdit, btnDel,btnMain,btnDone;
    
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ObservableList<Consultation> list;
    private Connection con = null;
 
    /**
     * Initializes the controller class.
     */
   public DashbordDocteurController(){
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();  //establishing connection to database
   }
    private void exit(ActionEvent event) throws SQLException {
        if (con != null) {
            con.close();
        }
        System.exit(0);
    }

    @FXML
    private void handleButtonClicks(ActionEvent ae) throws SQLException {
          if (ae.getSource() == btnMain) {     //home page
            if (con != null) {
                con.close();
            }
            FxmlLoader.loadStage("/JavaFXML/HomePage.fxml");
            ((Node) (ae.getSource())).getScene().getWindow().hide();
        } else if(ae.getSource() == btnEdit){  //Create button to activate update 
            showDisableButton(true);//show button desactive
            editableFields(true);//show the zone of text that are desactive
            txtReport.setText("");
            txtMalady.setText("");
            txtPresp.setText("");

            btnEdit.setDisable(true);
            lblEdit.setText("");
        } else if(ae.getSource() == btnCancel){   //Button to cancel editing
            showDisableButton(false);
            btnEdit.setDisable(false);
            TreeItem<Consultation> selectedItem = treeTableView.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                selectedItem.getValue();
            }
            //put data from treetableview in the textfields 
            showDetails(selectedItem);
            editableFields(false);
        } else if(ae.getSource() == btnDone){     //Button to update the database and show it in table of the selected user
            updatePressed();
        } else if(ae.getSource() == btnDel){      //Button to delete the patient (Only available if patient says he is cured)
            deleteEntry();
            //delete the same entry from table
            int index=treeTableView.getSelectionModel().getSelectedIndex();
            list.remove(index);
            //
            fieldClear();
            btnDel.setDisable(true);
            btnEdit.setDisable(true);
            showDisableButton(false);
            editableFields(false);
            lblEdit.setTextFill(Color.GREEN);
            lblEdit.setText("Patient Data Deleted");
        }
    }
    //Method to delete patient from database on pressing Del button, when patient says he is cured
    private void deleteEntry() throws SQLException {
        //String to get the first_name from label - Patient Name
        String pName = lblPname.getText();
        int spacePos = pName.indexOf(" ");
        if(spacePos>0){
            pName = pName.substring(0, spacePos);
        }
        
        String sql = "DELETE FROM patient where first_name=?";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, pName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    //Method to show or not-show the Update & Cancel button
    private void showDisableButton(boolean b) {
        btnDone.setVisible(b);
        btnCancel.setVisible(b);
    }
     //Method to disable the update-able fields
    private void editableFields(boolean b) {
        txtReport.setEditable(b);
        txtMalady.setEditable(b);
        txtPresp.setEditable(b);
    }

    //Method to clear all fields when nothing is selected in table
    private void fieldClear() {
        lblPname.setText("");
        txtReport.setText("");
        txtMalady.setText("");
        txtPresp.setText("");
        lblEdit.setText("");
    }
   //Method that get data from item tree and put it in the fields
    private void showDetails(TreeItem<Consultation> treeItem) {
        lblPname.setText(treeItem.getValue().getName());
        txtReport.setText(treeItem.getValue().getReport());
        txtMalady.setText(treeItem.getValue().getMalady());
        txtPresp.setText(treeItem.getValue().getPresprection());
        String cured = treeItem.getValue().getCured();
        if (cured.equals("Yes")){
            btnDel.setDisable(false);
            btnEdit.setDisable(true);
        }else{
            btnDel.setDisable(true);
            btnEdit.setDisable(false);
        }
    }
     //Method to put data from database and  put it in the treetable view
    private void TableView() throws SQLException {
        //set data in the tree table from the model consultation
        nameCol.setCellValueFactory(param -> param.getValue().getValue().nameProperty());
        reportCol.setCellValueFactory(param -> param.getValue().getValue().reportProperty());
        malCol.setCellValueFactory(param -> param.getValue().getValue().maladyProperty());
        presCol.setCellValueFactory(param -> param.getValue().getValue().presprectionProperty());
        cureCol.setCellValueFactory(param -> param.getValue().getValue().curedProperty());

        //Instantiate the Observable list
        list= FXCollections.observableArrayList();

        //Fill the table with data
        TreeItem<Consultation> root=new RecursiveTreeItem<Consultation>(list, RecursiveTreeObject::getChildren);
        treeTableView.setRoot(root);
        treeTableView.setShowRoot(false);
         
        //Take data from database and feel liste
        Patient p=new Patient();
        p.ListePatients(list);
        // set the name of the docter with login username
       
        Doctor d =new Doctor();
        d.setName(lblName, lblErrors);
    }
    
    
    
//method to modify the report for patient that he is say that no cured  
    private void updateFields() throws SQLException {
        //String to get the first_name from label - Patient Name
        String pName = lblPname.getText();
        int spacePos = pName.indexOf(" ");
        if(spacePos>0){
            pName = pName.substring(0, spacePos);
        }
        //
        String report = txtReport.getText();
        String malady = txtMalady.getText();
        String presprection = txtPresp.getText();
        String sql = "UPDATE patient SET report=?, malady=?, presprection=?, cured=? where first_name=?";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, report);
            preparedStatement.setString(2, malady);
            preparedStatement.setString(3, presprection);
            preparedStatement.setString(4, "Report Sent");
            preparedStatement.setString(5, pName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    //Method if update button is pressed
    private void updatePressed() throws SQLException {
        TreeItem<Consultation> selectedItem = treeTableView.getSelectionModel().getSelectedItem();
        String report = selectedItem.getValue().getReport();
        String malady = selectedItem.getValue().getMalady();
        String presp = selectedItem.getValue().getPresprection();
        //To check if the same text as before is found in the fields
        if(report.equals(txtReport.getText()) || malady.equals(txtMalady.getText()) || presp.equals(txtPresp.getText())){
            lblEdit.setTextFill(Color.TOMATO);
            lblEdit.setText("Please Edit All Fields!");
        }
        //To check if none of the fields is clear
        else if(txtReport.getText().equals("") || txtMalady.getText().equals("") || txtPresp.getText().equals("")){
            lblEdit.setTextFill(Color.TOMATO);
            lblEdit.setText("Fields can't be empty");
        }
        ///Continue updating if above conditions are fulfilled
        else {
            updateFields();
            //update the same fields in the selected table entry
            Consultation m = new Consultation(selectedItem.getValue().getName(),txtReport.getText(),txtMalady.getText(),txtPresp.getText(), "Report Sent");
            selectedItem.setValue(m);
            //
            btnDel.setDisable(true);
            btnEdit.setDisable(false);
            showDisableButton(false);
            editableFields(false);
            lblEdit.setTextFill(Color.GREEN);
            lblEdit.setText("All Fields Updated!");
        }
    }
 @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         if (con == null) {
            lblErrors.setTextFill(Color.TOMATO);
            lblErrors.setText("Server Error : Check");  //let user know error in establishing connection to database
        } else {
            lblErrors.setTextFill(Color.GREEN);
            lblErrors.setText("Server is up, All Good!");  //let the user know connection to database is established
        }

        btnDel.setDisable(true);
        btnEdit.setDisable(true);

        fieldClear();

        showDisableButton(false);

        try {
            TableView();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        editableFields(false);

        ////Event Handler to see if any table is selected
        treeTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showDetails(newValue);
            btnEdit.setDisable(false);
            editableFields(false);
            showDisableButton(false);
            lblEdit.setText("");
            TreeItem<Consultation> selectedItem = treeTableView.getSelectionModel().getSelectedItem();
            if(selectedItem.getValue().getCured().equals("Yes")){
                btnDel.setDisable(false);
                btnEdit.setDisable(true);
            }
        });


        ///SignIn on pressing enter on Keyboard
        txtPresp.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER){
                try {
                    updatePressed();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        
    }
    }    


