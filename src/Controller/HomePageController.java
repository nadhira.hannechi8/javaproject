/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ModelSys.FxmlLoader;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author nadhira
 */
public class HomePageController implements Initializable {

    @FXML
    private Pane pane1;
    @FXML
    private Pane pane2;
    @FXML
    private Pane pane3;
    @FXML
    private Pane pane4;
    @FXML
    private Pane pane5;
    @FXML
    private Label label2;
    @FXML
    private Label label1;

    @FXML
    private Button btnDocter;
    @FXML
    private Button btnPatient;


    /**
     * Initializes the controller class.
     */ //Constructor
    public HomePageController(){

    }
    //Redirection all button in login page
    @FXML
    public void handleButtonClicks(javafx.event.ActionEvent ae) {

        if (ae.getSource() == btnDocter) {
            System.out.println(ae.getSource());
            FxmlLoader.loadStage("/JavaFXML/LoginDocter.fxml"); 
            ((Node)(ae.getSource())).getScene().getWindow().hide(); 
        } else if (ae.getSource() == btnPatient) {
            FxmlLoader.loadStage("/JavaFXML/LoginPatient.fxml"); 
            ((Node)(ae.getSource())).getScene().getWindow().hide();
        }
    }
   


    //Set animation and background images
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        pane1.setStyle("-fx-background-image: url(\"media/image3.jpg\");"
                + "-fx-background-size: cover;");
        pane2.setStyle("-fx-background-image: url(\"media/image4.jpg\");"
                + "-fx-background-size: cover;");
        pane3.setStyle("-fx-background-image: url(\"media/image6.jpg\");"
        + "-fx-background-size: cover;");
        pane4.setStyle("-fx-background-image: url(\"media/4.jpg\");"
        + "-fx-background-size: cover;");
        backgroundAnimation();
    }

    


    @FXML
    private void exit(ActionEvent event) {
         System.exit(0);
    }
    
    //Method to control image transitions
    private void backgroundAnimation() {

       FadeTransition fadeTransition=new FadeTransition(Duration.seconds(5),pane4);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();

        //**********starts fadein-fadeout animations of images
        fadeTransition.setOnFinished(event -> {

            FadeTransition fadeTransition1=new FadeTransition(Duration.seconds(4),pane3);
            fadeTransition1.setFromValue(1);
            fadeTransition1.setToValue(0);
            fadeTransition1.play();

            fadeTransition1.setOnFinished(event1 -> {
                FadeTransition fadeTransition2=new FadeTransition(Duration.seconds(4),pane2);
                fadeTransition2.setFromValue(1);
                fadeTransition2.setToValue(0);
                fadeTransition2.play();

                fadeTransition2.setOnFinished(event2 -> {

                    FadeTransition fadeTransition0 =new FadeTransition(Duration.seconds(4),pane2);
                    fadeTransition0.setToValue(1);
                    fadeTransition0.setFromValue(0);
                    fadeTransition0.play();

                    fadeTransition0.setOnFinished(event3 -> {

                        FadeTransition fadeTransition11 =new FadeTransition(Duration.seconds(4),pane3);
                        fadeTransition11.setToValue(1);
                        fadeTransition11.setFromValue(0);
                        fadeTransition11.play();

                        fadeTransition11.setOnFinished(event4 -> {

                            FadeTransition fadeTransition22 =new FadeTransition(Duration.seconds(4),pane4);
                            fadeTransition22.setToValue(1);
                            fadeTransition22.setFromValue(0);
                            fadeTransition22.play();

                            fadeTransition22.setOnFinished(event5 -> {
                                backgroundAnimation();                                 
                            });

                        });

                    });

                });
            });

        });
    }
}

    