/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConnectionDB.ConnectionClass;
import Model.Login;
import ModelSys.FxmlLoader;
import ModelSys.Session;
import ModelSys.deptListe;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author nadhira
 */
public class LoginDocterController implements Initializable {
 private Connection con = null;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnGOBack;
    
 @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label lblStatus;
    @FXML
    private ComboBox<String> dept;
    /**
     * Initializes the controller class.
     */
    public LoginDocterController()
    {
        
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();  //establishing connection to database
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dept.getItems().clear();
        dept.getItems().addAll(deptListe.getDEPTS());
        if (con == null) {
            lblStatus.setTextFill(Color.TOMATO);
            lblStatus.setText("Server Error : Check");  //let user know error in establishing connection to database
        } else {
            lblStatus.setTextFill(Color.GREEN);
            lblStatus.setText("Server is up : Good to go");  //let the user know connection to database is established
        }

        ///SignIn on pressing enter on Keyboard
        password.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER){
                try {
                    if (login().equals("Success")) {
                        if (con != null) {
                            con.close();
                        }
                        System.out.println("success");
                        FxmlLoader.loadStage("/JavaFXML/DashbordDocteur.fxml");
                        ((Node) (e.getSource())).getScene().getWindow().hide();
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }    

    @FXML
    private void handleButtonClicks(javafx.event.ActionEvent ae) throws SQLException{
          if (ae.getSource() == btnGOBack) {   ///button event to go back to main page
            if (con != null) {
                con.close();
            }
            FxmlLoader.loadStage("/JavaFXML/HomePage.fxml");
            ((Node) (ae.getSource())).getScene().getWindow().hide();
        } else if (ae.getSource() == btnLogin) {    ////button to check signin and go to doctor's dashboard
            if (login().equals("Success")) {
              FxmlLoader.loadStage("/JavaFXML/DashbordDocteur.fxml");
              System.out.println("success");
             ((Node) (ae.getSource())).getScene().getWindow().hide();
                }
                
            }
        
    }

 //method to check login
     private String login() throws SQLException {
        Login l =new Login();
        String usernameT= username.getText();
        String passwordT=password.getText();
        String deptT=dept.getValue();
        ResultSet resultat = l.getLogin(usernameT,passwordT,deptT);

            if (!resultat.next()) {
                lblStatus.setTextFill(Color.TOMATO);
                lblStatus.setText("Enter Correct Username OR Password");
                username.clear();
                password.clear();
                return "Error";
            } else {
                lblStatus.setTextFill(Color.GREEN);
                lblStatus.setText("Login Successful..Redirecting..");
                Session.setUsername(username.getText());   ////save the successful login's username
                Session.setDepartment(dept.getValue()); 
                //System.out.println(Session.getDepartment());
                return "Success";
            }
}

   
   
    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }
    
}
