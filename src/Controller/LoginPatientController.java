/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConnectionDB.ConnectionClass;
import Model.Login;
import Model.LoginPatient;
import ModelSys.FxmlLoader;
import ModelSys.Session;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author nadhira
 */
public class LoginPatientController implements Initializable {

    
    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    
    @FXML
    private Button btnSignin;
    @FXML
    private Button btnSignup;
    @FXML
    private TextField txtUsername;
    @FXML
    private Button btnMain;
    @FXML
    private Label lblErrors;
    @FXML
    private PasswordField txtPassword;

    /**
     * Initializes the controller class.
     */
    public LoginPatientController() {
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
         // login when press ENTER
                txtPassword.setOnKeyPressed(e -> {
                    if(e.getCode() == KeyCode.ENTER){
                        try {
                            if (logIn().equals("Success")) {
                                if (con != null) {
                                    con.close();
                                }
                                System.out.println("success");
                                FxmlLoader.loadStage("/JavaFXML/DashbordDocteur.fxml");
                                ((Node) (e.getSource())).getScene().getWindow().hide();
                            }
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                    }
                });   
    }    

    @FXML
    private void handleButtonClicks(javafx.event.ActionEvent ae) throws SQLException {
        if (ae.getSource() == btnMain) {  //home page
            if (con != null) {
                con.close();
            }
            FxmlLoader.loadStage("/JavaFXML/HomePage.fxml");
            ((Node)(ae.getSource())).getScene().getWindow().hide();
        }
        else if (ae.getSource() == btnSignup) {   //new user
            if (con != null) {
                con.close();
            }
            FxmlLoader.loadStage("/JavaFXML/patientRegister.fxml");
            ((Node)(ae.getSource())).getScene().getWindow().hide();
        }
        else if (ae.getSource() == btnSignin) {   //dashbord user
            if (logIn().equals("Success")) {
                if (con != null) {
                    con.close();
                }
                FxmlLoader.loadStage("/JavaFXML/DashbordPatient.fxml");
                ((Node) (ae.getSource())).getScene().getWindow().hide();
            }
        }
    }
    //method to check login
    private String logIn() throws SQLException {
        LoginPatient l =new LoginPatient();
        String usernameT= txtUsername.getText();
        String passwordT=txtPassword.getText();
        ResultSet resultSet = l.getlogin(usernameT,passwordT);
            if (!resultSet.next()) {
                lblErrors.setTextFill(Color.TOMATO);
                lblErrors.setText("Enter Correct Username/Password");
                txtUsername.clear();
                txtPassword.clear();
                return "Error";
            } else {
                lblErrors.setTextFill(Color.GREEN);
                lblErrors.setText("Login Successful..Redirecting..");
                Session.setUsername(txtUsername.getText());   
                return "Success";
            }
    }
}
