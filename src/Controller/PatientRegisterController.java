/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConnectionDB.ConnectionClass;
import ModelSys.FxmlLoader;
import ModelSys.deptListe;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author nadhira
 */
public class PatientRegisterController implements Initializable {

    @FXML
    private ComboBox<String> dropDown;
    @FXML
    private Button btnCreate;
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtLastName;
    @FXML
    private DatePicker txtDob;
    @FXML
    private TextField txtPhoneNumber;
    @FXML
    private TextField txtUsername;
    @FXML
    private TextField txtPassword;
    @FXML
    private Button btnSignin;
    
    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    @FXML
    private Label lblErrors;
    @FXML
    private Label lblSaved;
  
   
    
    public PatientRegisterController() {
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();
    }



    @FXML
    public void handleButtonClicks(javafx.event.ActionEvent ae) throws SQLException {
        
            if (ae.getSource() == btnSignin) {    ///button to go back to signin page
            if (con != null) {
                con.close();
            }
            FxmlLoader.loadStage("/JavaFXML/LoginPatient.fxml");
            ((Node) (ae.getSource())).getScene().getWindow().hide();
        } else if (ae.getSource() == btnCreate) {      ///button to create patient account
            createPressed();
        }
    }

    //method when Create button is pressed
    private void createPressed() throws SQLException {
        //check if all fields are filled
        if (dropDown.getSelectionModel().isEmpty() || txtFirstName.getText().isEmpty() || txtLastName.getText().isEmpty() || txtDob.getValue().toString().equals("")
                || txtPhoneNumber.getText().isEmpty() || txtUsername.getText().isEmpty() || txtPassword.getText().isEmpty()) {
            lblErrors.setTextFill(Color.TOMATO);
            lblErrors.setText("Enter all details");
        }
        
        //check if username already exists in database
        else if(checkUsername(txtUsername.getText())){
            txtUsername.clear();
            txtPassword.clear();
            lblErrors.setTextFill(Color.TOMATO);
            lblErrors.setText("Username Exists!");
        }
        //create the account if all above conditions are fulfilled
        else {
            String verify = saveData();
            if (verify.equals("Success")){
                if (con != null) {
                    con.close();
                }
            }
        }
    }

    //Method to check if same username exists
    private boolean checkUsername(String text) throws SQLException {
        String sql = "SELECT * FROM patient Where username = ?";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, text);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return true;
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    //method to take the data entered and create an account
    private String saveData() throws SQLException {
        try {
            String st = "INSERT INTO patient (problem,department, first_name, last_name, dob, phone_number, username, password, report,malady,presprection,cured) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            String patientProb = dropDown.getValue();
            String coressDept= deptListe.setDepartment(patientProb);

            preparedStatement = con.prepareStatement(st);
            preparedStatement.setString(1, patientProb);
            preparedStatement.setString(2, coressDept);
            preparedStatement.setString(3, txtFirstName.getText());
            preparedStatement.setString(4, txtLastName.getText());
            preparedStatement.setString(5, txtDob.getValue().toString());
            preparedStatement.setString(6, txtPhoneNumber.getText());
            preparedStatement.setString(7, txtUsername.getText());
            preparedStatement.setString(8, txtPassword.getText());
            preparedStatement.setString(9, "No Result Yet");
            preparedStatement.setString(10, "No Result Yet");
            preparedStatement.setString(11, "No Result Yet");
            preparedStatement.setString(12, "Not Set");

            preparedStatement.executeUpdate();

            lblErrors.setTextFill(Color.GREEN);
            lblErrors.setText("Added Successfully");
          

            //*********to show the patient which Doctor has been assigned*********//
               String sql = "SELECT * FROM docteur Where departement = ?";
                try {
                    preparedStatement = con.prepareStatement(sql);
                    preparedStatement.setString(1, coressDept);
                    resultSet = preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        String doctorName = resultSet.getString(2);
                        String message = "Assigned to Dr. " +doctorName;
                        lblSaved.setText(message);
                       
                        lblSaved.setTextFill(Color.BLUE);
                    } else {
                        lblSaved.setTextFill(Color.TOMATO);
                        lblSaved.setText("No Doctor found");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            //******************************************************************************************//

            return "Success";
        } catch (SQLException ex) {
            ex.printStackTrace();
            if(ex.toString().contains("Incorrect integer")){
                lblErrors.setTextFill(Color.TOMATO);
                lblErrors.setText("Please Enter Valid Phone Number");
            }else {
                lblErrors.setTextFill(Color.TOMATO);
                lblErrors.setText(ex.getMessage());
            }
            return "Exception";
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    
   

    public void initialize(URL location, ResourceBundle resources) {
        ///Add Complains to dropdown...
        dropDown.getItems().clear();
        dropDown.getItems().addAll(deptListe.getProblem());

        lblSaved.setText("");
      

        if (con == null) {
            lblErrors.setTextFill(Color.TOMATO);
            lblErrors.setText("Server Error : Check");  //let user know error in establishing connection to database
        } else {
            lblErrors.setTextFill(Color.GREEN);
            lblErrors.setText("Server is up : Good to go");  //let the user know connection to database is established
        }

        ///Register on pressing enter on Keyboard
        txtPassword.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) {
                try {
                    createPressed();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        
    }

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }
    
}
