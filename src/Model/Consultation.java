/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author nadhira
 */
public class Consultation extends RecursiveTreeObject<Consultation> {
    private StringProperty name, report, malady, presprection, cured;

    public Consultation(String name, String report, String malady, String presprection, String cured) {
        this.name = new SimpleStringProperty(name) ;
        this.report = new SimpleStringProperty(report) ;
        this.malady = new SimpleStringProperty(malady) ;
        this.presprection = new SimpleStringProperty(presprection) ;
        this.cured = new SimpleStringProperty(cured) ;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getReport() {
        return report.get();
    }

    public StringProperty reportProperty() {
        return report;
    }

    public String getMalady() {
        return malady.get();
    }

    public StringProperty maladyProperty() {
        return malady;
    }

    public String getPresprection() {
        return presprection.get();
    }

    public StringProperty presprectionProperty() {
        return presprection;
    }

    public String getCured() {
        return cured.get();
    }

    public StringProperty curedProperty() {
        return cured;
    }
}
