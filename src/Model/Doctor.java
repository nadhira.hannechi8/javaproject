/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import ConnectionDB.ConnectionClass;
import ModelSys.Session;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

/**
 *
 * @author nadhira
 */
public class Doctor {
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ObservableList<Consultation> list;
    private Connection con = null;
    
    /**
     * Initializes the controller class.
     */
   public Doctor(){
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();  //establishing connection to database
   }
    public void setName(Label lblName,Label lblErrors) throws SQLException{
     String username = Session.getUsername();
     String sql = "SELECT * FROM docteur Where username = ?";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1,username);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String Name = resultSet.getString("name");
                System.out.println("name est "+Name);
                lblName.setText(Name);
            } else {
                lblErrors.setTextFill(Color.TOMATO);
                lblErrors.setText("Cannot Retreive Data. Server Error");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}
