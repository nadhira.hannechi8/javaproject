/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import ConnectionDB.ConnectionClass;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
/**
 *
 * @author nadhira
 */
public class Login {
        private PreparedStatement preparedStatement = null;
        private Connection con = null;
        private ResultSet resultSet = null;
    public ResultSet getLogin(String username,String password,String dept) throws SQLException{
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();
        if(con!=null)
        {
         String sql = "SELECT * FROM docteur Where username = ? and password = ? and departement=?";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, dept);
            resultSet = preparedStatement.executeQuery();
        }
            return resultSet;
    }
}

