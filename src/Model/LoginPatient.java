/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import ConnectionDB.ConnectionClass;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nadhira
 */
public class LoginPatient {
        private PreparedStatement preparedStatement = null;
        private Connection con = null;
        private ResultSet resultSet = null;

     public  ResultSet getlogin(String username,String password) throws SQLException {
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();
        if(con!=null)
        {
            String sql = "SELECT * FROM patient Where username = ? and password = ?";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
           
        }
         return resultSet;
    }
        
}
