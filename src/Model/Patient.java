/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import ConnectionDB.ConnectionClass;
import ModelSys.Session;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 *
 * @author nadhira
 */
public class Patient {
        private String username = Session.getUsername();
        private String department = Session.getDepartment();
        private Connection con = null;
        private PreparedStatement preparedStatement = null;
        private ResultSet resultSet = null;
        
    public Patient(){
        ConnectionClass c =new ConnectionClass();
        con = c.getConnection();  //establishing connection to database
   }
    public void ListePatients(ObservableList<Consultation> list) throws SQLException{
         
        String sql = "SELECT * FROM patient Where department = ?";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, department);
            resultSet = preparedStatement.executeQuery();
                while(resultSet.next()) {
                    String pName = resultSet.getString("first_name") + " " + resultSet.getString("last_name");
                    list.add(new Consultation(pName, resultSet.getString("report"), resultSet.getString("malady"),  ///Push the data collected from database to ModelTable
                            resultSet.getString("presprection"), resultSet.getString("cured")));                       //and store in list
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
            
    
}
