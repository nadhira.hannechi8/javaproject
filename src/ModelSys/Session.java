/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelSys;

/**
 *
 * @author nadhira
 */
public class Session {
    private static String username = "";
    private static String department = "";


    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Session.username = username;
    }

    public static String getDepartment() {
        return department;
    }

    public static void setDepartment(String department) {
        Session.department = department;
    }
}
