/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import ConnectionDB.ConnectionClass;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author nadhira
 */
public class ProjetJava extends Application {

    private double xOffset = 0;
    private double yOffset = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/JavaFXML/HomePage.fxml")); //load the main page view
            primaryStage.setTitle("Welcome");
            primaryStage.setScene(new Scene(root, 1200, 700));

            primaryStage.getIcons().add(new Image("media/icon.png"));
            primaryStage.getScene().getStylesheets().addAll(getClass().getResource("/Style/style.css").toExternalForm());

            ////**********Move Window on Mouse Drag anywhere on the screen*****/////
            primaryStage.initStyle(StageStyle.TRANSPARENT);
            root.setOnMousePressed(event -> {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            });
            ConnectionClass c =new ConnectionClass();
            System.out.println(c.getConnection());
            //move around here
            root.setOnMouseDragged(event -> {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            });

            primaryStage.show();
            primaryStage.setMaximized(false);
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main (String[]args){
            launch(args);
        }
}